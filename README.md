## Features

- [x] Database ([typeorm](https://www.yarnjs.com/package/typeorm)).
- [x] Seeding ([typeorm-seeding](https://www.yarnjs.com/package/typeorm-seeding)).
- [x] Config Service ([@nestjs/config](https://www.yarnjs.com/package/@nestjs/config)).
- [x] Mailing ([nodemailer](https://www.yarnjs.com/package/nodemailer), [@nestjs-modules/mailer](https://www.yarnjs.com/package/@nestjs-modules/mailer)).
- [x] Sign in and sign up via email.
- [x] Admin and User roles.
- [x] I18N ([nestjs-i18n](https://www.yarnjs.com/package/nestjs-i18n)).
- [x] File uploads.
- [x] Swagger.
- [ ] E2E and units tests.
- [x] Docker.

## Quick run

```bash
cp config/envs/env-example config/envs/.env
docker-compose up -d
```

For check status run

```bash
docker-compose logs
```

## Comfortable development

```bash
cp envs/env-example envs/.env
```

Change `DATABASE_HOST=postgres` to `DATABASE_HOST=localhost`

Change `MAIL_HOST=maildev` to `MAIL_HOST=localhost`

Run additional container:

```bash
docker-compose up -d database adminer maildev redis
```

```bash
yarn

yarn migration:run

yarn seed:run

yarn start:dev
```

## Database utils

Generate migration

```bash
yarn migration:generate -- CreateNameTable
```

Run migration

```bash
yarn migration:run
```

Revert migration

```bash
yarn migration:revert
```

Drop all tables in database

```bash
yarn schema:drop
```

Run seed

```bash
yarn seed:run
```

## Tests

```bash
# unit tests
yarn run test

# e2e tests
yarn run test:e2e
```

## Tests in Docker

```bash
docker-compose -f docker-compose.ci.yaml --env-file env-example -p ci up --build --exit-code-from api && docker-compose -p ci rm -svf
```

## Test benchmarking

```bash
docker run --rm jordi/ab -n 100 -c 100 -T application/json -H "Authorization: Bearer USER_TOKEN" -v 2 http://<server_ip>:3000/api/v1/users
```
