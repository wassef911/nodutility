import {
  WebSocketGateway,
  WsException,
  BaseWsExceptionFilter,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { UseFilters } from '@nestjs/common';
import { NAMESPACE } from '../chat.enum';
import { BaseGateway } from './base.gateway';
import { ChatUserService } from '../services/chatUser.service';
import { RoomService } from '../services/room.service';
import { MessageService } from '../services/message.service';

@WebSocketGateway({ namespace: NAMESPACE + '/chat' })
export class OfficerGateway extends BaseGateway {
  constructor(
    protected chatUserService: ChatUserService,
    protected roomService: RoomService,
    protected messageService: MessageService,
  ) {
    super(chatUserService, roomService, messageService);
  }

  @UseFilters(new BaseWsExceptionFilter())
  async handleConnection(@ConnectedSocket() client: Socket) {
    try {
      const { chatUser } = this.getHandshakeQuery(client);
      /**
       * find or create user
       */
      const user = await this.chatUserService.findOne({
        phone_number: chatUser.phone_number,
      });
      if (!user) await this.chatUserService.create(chatUser);
    } catch (error) {
      throw new WsException(error.message);
    }
  }
}
