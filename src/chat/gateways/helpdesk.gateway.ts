import {
  WebSocketGateway,
  WsException,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { NAMESPACE } from '../chat.enum';
import { BaseGateway } from './base.gateway';
import { ChatUserService } from '../services/chatUser.service';
import { RoomService } from '../services/room.service';
import { MessageService } from '../services/message.service';
import { Room } from '../entities/room.entity';

@WebSocketGateway({ namespace: NAMESPACE + '/helpdesk' })
export class HelpDeskGateway extends BaseGateway {
  constructor(
    protected chatUserService: ChatUserService,
    protected roomService: RoomService,
    protected messageService: MessageService,
  ) {
    super(chatUserService, roomService, messageService);
  }

  async handleConnection(@ConnectedSocket() client: Socket) {
    try {
      const { chatUser } = this.getHandshakeQuery(client);
      const roomId = chatUser.phone_number;
      /**
       * find or create user
       */
      let user = await this.chatUserService.findOne({
        phone_number: chatUser.phone_number,
      });
      if (!user) user = await this.chatUserService.create(chatUser);

      if (roomId) {
        // if client is a citizen/officer
        /**
         * find or create room
         */
        let room: Room = await this.roomService.findOne({ name: roomId });
        if (!room)
          room = await this.roomService.create({
            name: roomId,
            isForHelp: true,
          });

        /**
         * join him to room
         * emit to helpedesk users
         */
        room = await this.roomService.update(room.id, {
          ...room,
          chatUsers: [...room.chatUsers, user],
        });
        await client.join(roomId);
      }
    } catch (error) {
      throw new WsException(error.message);
    }
  }
}
