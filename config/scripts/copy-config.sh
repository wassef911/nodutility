if [ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master" ]
then
   scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r ./config/** $USERNAME@$HOST_PROD:/home/helpdesk/
else
   scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r ./config/** $USERNAME@$HOST_DEV:/home/helpdesk/
fi